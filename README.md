# DBMetaDB

#### 介绍
数据库元信息数据库，用于实现对数据库元信息的重新组织。

例如我们有一个管理系统数据库，里面有很多的表（例如 sms_202101,sms_2002102...按月份分表的短信表），我们需要对表进行管理，如果用数据库管理客户端，我们只能按照单表的方式，有时候我们需要知道，我们究竟有多少张sms表，这时候我们需要查询数据库的元信息，但是不同的数据库管理系统往往不同，查询也不是很方便，另外一个方面，就是无法对这些表进行统一的备注和管理。这个项目就是为了解决这个问题，例如，我们可以建立第一个sms表，然后把sms_{DDDDDD}，这种模式的表全部添加到这个sms里面，我们就能一目了然的知道我们数据库当前的结构信息。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
