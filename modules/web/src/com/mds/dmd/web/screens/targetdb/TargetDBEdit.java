package com.mds.dmd.web.screens.targetdb;

import com.haulmont.cuba.gui.screen.*;
import com.mds.dmd.entity.TargetDB;

@UiController("dmd_TargetDB.edit")
@UiDescriptor("target-db-edit.xml")
@EditedEntityContainer("targetDBDc")
@LoadDataBeforeShow
public class TargetDBEdit extends StandardEditor<TargetDB> {
}