package com.mds.dmd.web.screens.mview;

import com.haulmont.chile.core.datatypes.impl.EnumClass;
import com.mds.dmd.entity.DBType;

import javax.annotation.Nullable;

public enum NameCaseEnum implements EnumClass<String> {
    None("None"),
    UpperCase("UpperCase"),
    LowerCase("LowerCase");


    private String id;

    NameCaseEnum(String value) {
        this.id = value;
    }

    @Override
    public String getId() {
        return id;
    }

    @Nullable
    public static NameCaseEnum fromId(String id) {
        for (NameCaseEnum at : NameCaseEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
