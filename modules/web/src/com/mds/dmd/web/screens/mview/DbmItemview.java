package com.mds.dmd.web.screens.mview;

import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.app.core.inputdialog.DialogActions;
import com.haulmont.cuba.gui.app.core.inputdialog.DialogOutcome;
import com.haulmont.cuba.gui.app.core.inputdialog.InputParameter;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.mds.dmd.core.service.ConsoleService;
import com.mds.dmd.entity.DBTable;
import com.mds.dmd.entity.TargetDB;
import com.mds.dmd.events.ConsoleUpdateEvent;
import com.mds.dmd.core.service.PDBMetaService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.Set;

@UiController("dmd_DbmItemview")
@UiDescriptor("dbm_ItemView.xml")
@LoadDataBeforeShow
public class DbmItemview extends ScreenFragment {

    @Inject
    private Dialogs dialogs;
    @Inject
    private Filter ftTables;
    @Inject
    private GroupTable<DBTable> gtbTables;
    @Inject
    private TextArea<String> txaConsole;
    @Inject
    private PDBMetaService pDBMetaService;
    @Inject
    private CollectionLoader<DBTable> tblsLoader;
    @Inject
    private ConsoleService consoleService;
    @Inject
    protected Messages messages;

    private TargetDB thisDB;

    public void setTargetDB(TargetDB db){
        Assert.notNull(db,"目标数据库未设置!");
        thisDB = db;
    }

    @Subscribe
    public void onInit(InitEvent event) {
       tblsLoader.setParameter("db", thisDB);
        tblsLoader.load();
    }

    @Subscribe("btnRefresh")
    public void onBtnRefreshClick(Button.ClickEvent event) throws Exception {
        pDBMetaService.refreshCatalog(thisDB);
        tblsLoader.load();
    }

    @Subscribe("btnToString")
    public void onBtnToStringClick(Button.ClickEvent event) {
        dialogs.createInputDialog(this)
                .withCaption("生成表名字符串选项")
                .withParameters(
                        InputParameter.booleanParameter("includeSchema").withCaption(LS("DIV.IncludeSchema")),
                        InputParameter.booleanParameter("quoted").withCaption(LS("DIV.Quoted")),
                        InputParameter.enumParameter("nameCase",NameCaseEnum.class).withCaption(LS("DIV.Case.Caption")),
                        InputParameter.stringParameter("splitter").withCaption(LS("DIV.Splitter"))
                       )
                .withActions(DialogActions.OK_CANCEL)
                .withCloseListener(
                        ce -> {
                            if(ce.closedWith(DialogOutcome.OK)){
                                boolean isQuoted = ce.getValue("quoted");
                                boolean isIncludeSchema=ce.getValue("includeSchema");
                                NameCaseEnum nameCase=ce.getValue("nameCase");
                                String splitter=ce.getValue("splitter");

                                Set<DBTable> selected=gtbTables.getSelected();
                                StringBuilder sb=new StringBuilder();

                                for (DBTable t:selected) {
                                    if(isIncludeSchema){
                                        String sn = thisDB.getDbSchema();
                                        if(nameCase==NameCaseEnum.LowerCase){
                                            sn = sn.toLowerCase();
                                        }else if(nameCase==NameCaseEnum.UpperCase){
                                            sn = sn.toUpperCase();
                                        }
                                        if(isQuoted){
                                            sb.append("\""+ sn +"\".");
                                        }else{
                                            sb.append(sn+".");
                                        }
                                    }

                                    String tn = t.getTableName();
                                    if(nameCase==NameCaseEnum.LowerCase){
                                        tn = tn.toLowerCase();
                                    }else if(nameCase==NameCaseEnum.UpperCase){
                                        tn = tn.toUpperCase();
                                    }

                                    if(isQuoted){
                                        sb.append("\""+tn+"\"");
                                    }else{
                                        sb.append(tn);
                                    }

                                    if(StringUtils.isEmpty(splitter)){
                                        sb.append(" ");
                                    }else {
                                        sb.append(splitter);
                                    }
                                }

                                consoleService.println(sb.toString());
                            }
                        }
                )
                .show();

    }

    @EventListener
    public void onApplicationEvent(ConsoleUpdateEvent event) {
        txaConsole.setValue(event.getAllMsg());
    }

    private String LS(String key){
        return messages.getMessage(this.getClass(),key);
    }
}