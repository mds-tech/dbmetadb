package com.mds.dmd.web.screens.mview;

import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Fragments;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.TabSheet;
import com.haulmont.cuba.gui.screen.*;
import com.mds.dmd.entity.TargetDB;

import javax.inject.Inject;

@UiController("dmd_MainViewer")
@UiDescriptor("main-viewer.xml")
@LoadDataBeforeShow
public class MainViewer extends Screen {
    @Inject
    private Notifications notifications;

    @Inject
    private LookupField<TargetDB> lufDBs;

    @Inject
    private Messages messages;

    @Inject
    private TabSheet tbsMain;

    @Inject
    private Fragments fragments;

    @Subscribe("btnOpen")
    public void onBtnOpenClick(Button.ClickEvent event) throws Exception {
        TargetDB db=lufDBs.getValue();
        if(db==null){
            notifications.create(Notifications.NotificationType.ERROR)
                    .withCaption(messages.getMainMessage("ntf_dialog.title.tip"))
                    .withDescription(messages.getMessage(this.getClass(), "MV.not_select_db"))
                    .show();
        }else{
            DbmItemview itemview = fragments.create(this,DbmItemview.class);
            itemview.setTargetDB(db);
            tbsMain.addTab(db.getDbName(),itemview.getFragment());
        }
    }
}