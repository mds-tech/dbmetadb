package com.mds.dmd.web.screens.targetdb;

import com.haulmont.cuba.gui.screen.*;
import com.mds.dmd.entity.TargetDB;

@UiController("dmd_TargetDB.browse")
@UiDescriptor("target-db-browse.xml")
@LookupComponent("targetDBsTable")
@LoadDataBeforeShow
public class TargetDBBrowse extends StandardLookup<TargetDB> {
}