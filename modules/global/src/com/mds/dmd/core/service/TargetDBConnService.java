package com.mds.dmd.core.service;

import com.mds.dmd.entity.TargetDB;

import java.sql.Connection;
import java.sql.SQLException;

public interface TargetDBConnService {
    String NAME = "dmd_TargetDBConnService";

    Connection createConn(TargetDB db) throws ClassNotFoundException, SQLException;
}