package com.mds.dmd.core.service;

public interface ConsoleService {
    String NAME = "dmd_ConsoleService";

    void println(String msg);

}