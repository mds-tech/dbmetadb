package com.mds.dmd.core.service;

import com.mds.dmd.entity.TargetDB;

public interface PDBMetaService {
    String NAME = "dmd_PDBMetaService";

    /**
     * 重新从目标数据库中读取Catalog
     * @param db
     * @throws Exception
     */
    void refreshCatalog(TargetDB db) throws Exception;
}