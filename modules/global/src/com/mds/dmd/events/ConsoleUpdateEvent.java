package com.mds.dmd.events;

import com.haulmont.addon.globalevents.GlobalApplicationEvent;
import com.haulmont.addon.globalevents.GlobalUiEvent;
import org.springframework.context.ApplicationEvent;

public class ConsoleUpdateEvent extends GlobalApplicationEvent implements GlobalUiEvent {
    private String updateMsg;

    private String allMsg;

    public String getUpdateMsg(){
        return this.updateMsg;
    }

    public String getAllMsg(){
        return this.allMsg;
    }

    public ConsoleUpdateEvent(Object source, String updatedMsg,String allMsg) {
        super(source);

        this.updateMsg=updatedMsg;
        this.allMsg=allMsg;
    }

    
}
