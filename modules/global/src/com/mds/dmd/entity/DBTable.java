package com.mds.dmd.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;

@Table(name = "DMD_DB_TABLE")
@Entity(name = "dmd_DBTable")
public class DBTable extends StandardEntity {
    private static final long serialVersionUID = 8879223730681856609L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TARGET_DB_ID")
    private TargetDB targetDB;

    @Column(name = "TABLE_NAME")
    private String tableName;

    @Column(name = "TABLE_ALIAS")
    private String tableAlias;

    @Column(name = "COMMENT_")
    private String comment;

    @Column(name = "HAS_PK")
    private Boolean hasPK;

    @Column(name = "IS_VIEW")
    private Boolean isView;

    public Boolean getIsView() {
        return isView;
    }

    public void setIsView(Boolean isView) {
        this.isView = isView;
    }

    public Boolean getHasPK() {
        return hasPK;
    }

    public void setHasPK(Boolean hasPK) {
        this.hasPK = hasPK;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public TargetDB getTargetDB() {
        return targetDB;
    }

    public void setTargetDB(TargetDB targetDB) {
        this.targetDB = targetDB;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}