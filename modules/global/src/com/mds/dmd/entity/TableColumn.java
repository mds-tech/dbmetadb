package com.mds.dmd.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "DMD_TABLE_COLUMN")
@Entity(name = "dmd_TableColumn")
public class TableColumn extends StandardEntity {
    private static final long serialVersionUID = 6140502167402963332L;

    @Lookup(type = LookupType.DROPDOWN, actions = {})
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "DB_TABLE_ID")
    private DBTable dbTable;

    @Column(name = "COL_NAME")
    private String colName;

    @Column(name = "COL_ALIAS")
    private String colAlias;

    @Column(name = "COL_TYPE")
    private String colType;

    @Column(name = "IS_PK")
    private Boolean isPK;

    public Boolean getIsPK() {
        return isPK;
    }

    public void setIsPK(Boolean isPK) {
        this.isPK = isPK;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColAlias() {
        return colAlias;
    }

    public void setColAlias(String colAlias) {
        this.colAlias = colAlias;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public DBTable getDbTable() {
        return dbTable;
    }

    public void setDbTable(DBTable dbTable) {
        this.dbTable = dbTable;
    }
}