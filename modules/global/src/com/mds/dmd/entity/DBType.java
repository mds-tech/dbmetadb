package com.mds.dmd.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum DBType implements EnumClass<String> {

    ORACLE("ORACLE"),
    MYSQL("MYSQL");

    private String id;

    DBType(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static DBType fromId(String id) {
        for (DBType at : DBType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}