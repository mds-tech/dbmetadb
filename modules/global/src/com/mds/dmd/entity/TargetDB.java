package com.mds.dmd.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "DMD_TARGET_DB")
@Entity(name = "dmd_TargetDB")
@NamePattern("%s|dbName")

public class TargetDB extends StandardEntity {
    private static final long serialVersionUID = -3343521103282179018L;

    @Column(name = "DB_NAME", nullable = false)
    @NotNull
    private String dbName;

    @Column(name = "DB_SERVER")
    private String dbUrl;

    @Column(name = "DB_USER_NAME")
    private String dbUserName;

    @Column(name = "DB_PASSWORD")
    private String dbPassword;

    @Column(name = "DB_TYPE", nullable = false)
    @NotNull
    private String dbType;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "DB_SCHEMA")
    private String dbSchema;

    public String getDbSchema() {
        return dbSchema;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public void setDbType(DBType dbType) {
        this.dbType = dbType == null ? null : dbType.getId();
    }

    public DBType getDbType() {
        return dbType == null ? null : DBType.fromId(dbType);
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}