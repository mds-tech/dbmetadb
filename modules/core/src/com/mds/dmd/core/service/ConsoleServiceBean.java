package com.mds.dmd.core.service;

import com.haulmont.cuba.core.global.Events;
import com.mds.dmd.events.ConsoleUpdateEvent;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(ConsoleService.NAME)
public class ConsoleServiceBean implements ConsoleService {
    private StringBuffer  buffer;
    final int BUFFER_SIZE=1024;
    final int MAX_SIZE=BUFFER_SIZE * 1024 * 10;
    @Inject
    Events events;

    public ConsoleServiceBean(){
        buffer = new StringBuffer(BUFFER_SIZE);
    }

    @Override
    public void println(String msg) {
        if(buffer.length()>MAX_SIZE){
            buffer.delete(0,msg.length() + 1);
        }
        buffer.append(msg);
        buffer.append("\n");

        ConsoleUpdateEvent e = new ConsoleUpdateEvent(this,msg,buffer.toString());
        events.publish(e);
    }


}