package com.mds.dmd.core.config;

import com.haulmont.cuba.core.config.Config;
import com.haulmont.cuba.core.config.Property;
import com.haulmont.cuba.core.config.Source;
import com.haulmont.cuba.core.config.SourceType;
import com.haulmont.cuba.core.config.defaults.Default;

@Source(type= SourceType.DATABASE)
public interface DBDriverConfig extends Config {
    @Property("dbDriver.MYSQL")
    @Default("com.mysql.jdbc.Driver")
    String getMysql();

    @Property("dbDriver.ORACLE")
    @Default("oracle.jdbc.OracleDriver")
    String getOracle();

}
