alter table DMD_DB_TABLE add constraint FK_DMD_DB_TABLE_ON_TARGET_DB foreign key (TARGET_DB_ID) references DMD_TARGET_DB(ID);
create index IDX_DMD_DB_TABLE_ON_TARGET_DB on DMD_DB_TABLE (TARGET_DB_ID);
