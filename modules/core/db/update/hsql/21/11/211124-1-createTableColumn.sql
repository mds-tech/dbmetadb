create table DMD_TABLE_COLUMN (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DB_TABLE_ID varchar(36) not null,
    COL_NAME varchar(255),
    COL_ALIAS varchar(255),
    COL_TYPE varchar(255),
    IS_PK boolean,
    --
    primary key (ID)
);