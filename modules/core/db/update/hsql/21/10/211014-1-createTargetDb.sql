create table DMD_TARGET_DB (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DB_NAME varchar(255) not null,
    DB_SERVER varchar(255),
    DB_USER_NAME varchar(255),
    DB_PASSWORD varchar(255),
    DB_OPTIONS varchar(255),
    DB_TYPE varchar(50) not null,
    --
    primary key (ID)
);