create table DMD_TARGET_DB (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    DB_NAME varchar(255) not null,
    DB_SERVER varchar(255),
    DB_USER_NAME varchar(255),
    DB_PASSWORD varchar(255),
    DB_TYPE varchar(50) not null,
    REMARK varchar(255),
    DB_SCHEMA varchar(255),
    --
    primary key (ID)
);