create table DMD_DB_TABLE (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    TARGET_DB_ID varchar(32),
    TABLE_NAME varchar(255),
    TABLE_ALIAS varchar(255),
    COMMENT_ varchar(255),
    HAS_PK boolean,
    IS_VIEW boolean,
    --
    primary key (ID)
);