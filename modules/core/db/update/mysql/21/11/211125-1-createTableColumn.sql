create table DMD_TABLE_COLUMN (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    DB_TABLE_ID varchar(32) not null,
    COL_NAME varchar(255),
    COL_ALIAS varchar(255),
    COL_TYPE varchar(255),
    IS_PK boolean,
    --
    primary key (ID)
);