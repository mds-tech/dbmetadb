-- begin DMD_TARGET_DB
create table DMD_TARGET_DB (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DB_NAME varchar(255) not null,
    DB_SERVER varchar(255),
    DB_USER_NAME varchar(255),
    DB_PASSWORD varchar(255),
    DB_TYPE varchar(50) not null,
    REMARK varchar(255),
    DB_SCHEMA varchar(255),
    --
    primary key (ID)
)^
-- end DMD_TARGET_DB
-- begin DMD_DB_TABLE
create table DMD_DB_TABLE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TARGET_DB_ID varchar(36),
    TABLE_NAME varchar(255),
    TABLE_ALIAS varchar(255),
    COMMENT_ varchar(255),
    HAS_PK boolean,
    IS_VIEW boolean,
    --
    primary key (ID)
)^
-- end DMD_DB_TABLE
-- begin DMD_TABLE_COLUMN
create table DMD_TABLE_COLUMN (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DB_TABLE_ID varchar(36) not null,
    COL_NAME varchar(255),
    COL_ALIAS varchar(255),
    COL_TYPE varchar(255),
    IS_PK boolean,
    --
    primary key (ID)
)^
-- end DMD_TABLE_COLUMN
